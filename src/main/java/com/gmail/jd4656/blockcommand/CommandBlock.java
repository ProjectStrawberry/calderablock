package com.gmail.jd4656.blockcommand;

import com.gmail.jd4656.InventoryManager.InventoryClickHandler;
import com.gmail.jd4656.InventoryManager.InventoryManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CommandBlock implements CommandExecutor {
    private BlockCommand plugin;
    private Map<UUID, Long> confirm = new HashMap<>();

    CommandBlock(BlockCommand p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }

        Player player = (Player) sender;

        if (confirm.containsKey(player.getUniqueId()) && confirm.get(player.getUniqueId()) < System.currentTimeMillis()) confirm.remove(player.getUniqueId());

        if (args.length > 0 && args[0].equals("reload")) {
            if (!sender.hasPermission("blockcommand.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            confirm = new HashMap<>();
            plugin.reloadConfig();
            plugin.config = plugin.getConfig();
            plugin.loadBlocks();
            sender.sendMessage(ChatColor.DARK_GREEN + "Configuration reloaded.");
            return true;
        }

        if (!sender.hasPermission("blockcommand.use")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        if (args.length > 0 && args[0].equals("cancel")) {
            confirm.remove(player.getUniqueId());
            sender.sendMessage(ChatColor.GOLD + "Cancelled turning items into blocks.");
            return true;
        }

        PlayerInventory inv = player.getInventory();
        InventoryManager manager = buildManager(player);

        if (manager == null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Error turning items into blocks.");
            return true;
        }

        manager.withEventHandler(new InventoryClickHandler() {
            @Override
            public void handle(InventoryClickEvent event) {
                Player player = (Player) event.getWhoClicked();
                ItemStack clicked = event.getCurrentItem();
                if (event.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY) && !manager.inventory.equals(event.getClickedInventory())) {
                    event.setCancelled(true);
                    return;
                }
                if (clicked.getType().equals(Material.AIR)) return;

                event.setCancelled(true);

                Material clickedMaterial = clicked.getType();
                Map<Material, Integer> count = countItems(player);

                for (String[] keys : plugin.blockList) {
                    Material curMaterial = Material.matchMaterial(keys[2]);
                    Material fromMaterial = Material.matchMaterial(keys[0]);
                    if (!clickedMaterial.equals(curMaterial)) continue;

                    int stackSize;
                    int amountPer;
                    try {
                        stackSize = Integer.parseInt(keys[1]);
                        amountPer = Integer.parseInt(keys[3]);
                    } catch (NumberFormatException ignored) {
                        player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Error turning items into blocks.");
                        player.closeInventory();
                        return;
                    }

                    int amount = count.get(fromMaterial);
                    double itemsToReturn = 0;

                    if (!event.isShiftClick() && amount > 64) {
                        itemsToReturn = (amount - 64);
                        amount = 64;
                    }

                    double itemsToAdd = ((double) amount / stackSize);
                    String decimalStr = String.valueOf(itemsToAdd);
                    itemsToAdd = Math.floor(itemsToAdd) * amountPer;
                    double decimal = 0.0;
                    if (decimalStr.contains(".")) {
                        try {
                            decimal = Double.parseDouble(decimalStr.substring(decimalStr.indexOf(".")));
                        } catch (NumberFormatException ignored) {}
                        itemsToReturn += Math.round((decimal * stackSize));
                    }

                    if (itemsToAdd < 1 || !inv.contains(fromMaterial, amount)) {
                        player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have enough items to turn into blocks.");
                        player.closeInventory();
                        return;
                    }

                    inv.remove(fromMaterial);
                    while (itemsToAdd > 0) {
                        ItemStack curItem = new ItemStack(clickedMaterial);
                        if (itemsToAdd >= 64) {
                            curItem.setAmount(64);
                            itemsToAdd -= 64;
                        } else {
                            curItem.setAmount((int) itemsToAdd);
                            itemsToAdd -= itemsToAdd;
                        }
                        inv.addItem(curItem);
                    }
                    if (itemsToReturn > 0) {
                        ItemStack item = new ItemStack(fromMaterial);
                        item.setAmount((int) itemsToReturn);
                        inv.addItem(item);
                    }

                    player.closeInventory();

                    InventoryManager newManager = buildManager(player);
                    if (newManager == null) return;
                    newManager.withEventHandler(this);
                    newManager.show(player);
                    return;
                }
            }
        });

        manager.show(player);
        return true;
    }

    private InventoryManager buildManager(Player player) {
        Map<Material, Integer> count = countItems(player);

        InventoryManager manager = new InventoryManager("Turn items into blocks", 54, plugin);

        int invPos = 0;

        for (Map.Entry<Material, Integer> entry : count.entrySet()) {
            Material curMaterial = entry.getKey();
            int amount = entry.getValue();

            for (String[] key : plugin.blockList) {
                if (!Material.matchMaterial(key[0]).equals(curMaterial)) continue;
                Material newMaterial = Material.matchMaterial(key[2]);

                int stackSize;
                int amountPer;
                try {
                    stackSize = Integer.parseInt(key[1]);
                    amountPer = Integer.parseInt(key[3]);
                } catch (NumberFormatException ignored) {
                    return null;
                }

                double itemsToAdd = ((double) amount / stackSize);
                itemsToAdd = Math.floor(itemsToAdd) * amountPer;

                ItemStack invItem = new ItemStack(newMaterial, (int) itemsToAdd);

                manager.withItem(invPos, invItem);
                invPos++;
            }
        }

        return manager;
    }

    private Map<Material, Integer> countItems(Player player) {
        PlayerInventory inv = player.getInventory();
        ItemStack[] items = inv.getContents();
        Map<Material, Integer> count = new HashMap<>();

        for (ItemStack curItem : items) {
            if (curItem == null) continue;
            if (!count.containsKey(curItem.getType())) count.put(curItem.getType(), 0);
            count.replace(curItem.getType(), count.get(curItem.getType()) + curItem.getAmount());
        }

        return count;
    }
}
