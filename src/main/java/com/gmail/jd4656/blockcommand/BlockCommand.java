package com.gmail.jd4656.blockcommand;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.LinkedList;
import java.util.List;

public class BlockCommand extends JavaPlugin {
    private BlockCommand plugin;
    List<String[]> blockList = new LinkedList<>();
    FileConfiguration config = null;

    @Override
    public void onEnable() {
        plugin = this;
        this.saveDefaultConfig();
        config = this.plugin.getConfig();
        config.options().copyDefaults(false);
        plugin.saveConfig();

        loadBlocks();

        this.getCommand("block").setExecutor(new CommandBlock(this));

        this.getLogger().info("BlockCommand loaded.");
    }

    void loadBlocks() {
        List<String> blocks = plugin.config.getStringList("blocks");
        blockList = new LinkedList<>();

        for (String key : blocks) {
            String[] splitKey = key.split(":");
            Material curMaterial = Material.matchMaterial(splitKey[0]);
            if (curMaterial == null) {
                plugin.getLogger().warning("Invalid material in config.yml: " + splitKey[0]);
                continue;
            }
            if (Material.matchMaterial(splitKey[2]) == null) {
                plugin.getLogger().warning("Invalid material in config.yml: " + splitKey[2]);
                continue;
            }
            try {
                Integer.parseInt(splitKey[1]);
            } catch (NumberFormatException e) {
                plugin.getLogger().warning("Invalid number in config.yml: " + splitKey[1]);
                continue;
            }
            try {
                Integer.parseInt(splitKey[3]);
            } catch (NumberFormatException e) {
                plugin.getLogger().warning("Invalid number in config.yml: " + splitKey[3]);
                continue;
            }
            blockList.add(splitKey);
        }
    }
}
